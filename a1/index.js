
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(response => {
    let array = response.map(item => item.title)
    console.log(array);
})

// let newArray = response.map(justTitle => justTitle.title);
// console.log(newArray);

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then( response => {
    console.log(`The item "${response.title}" on the list has a status of ${response.completed}`)
});

fetch("https://jsonplaceholder.typicode.com/todos",
{
    method: "POST",
    headers: {
        "content-type" : "application/json"
    },
    body: JSON.stringify({
        completed: false,
        title: "Created to do list item",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))




fetch("https://jsonplaceholder.typicode.com/todos/1",
{
    method: "PUT",
    headers: {
        "content-type" : "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        status: "Pending",
        title: "Updated to do list item",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))



fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "content-type" : "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "07/09/21",
        status: "Complete"
    })
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE",
}).then(response => response.json())
.then(json => console.log(json))